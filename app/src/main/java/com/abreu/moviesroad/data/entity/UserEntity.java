package com.abreu.moviesroad.data.entity;

public class UserEntity {
    private long id;
    private String username;
    private String name;
    private String role;
    private String accessToken;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
