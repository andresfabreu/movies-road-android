package com.abreu.moviesroad.data.entity;

public class MovieDetailEntity {
    private long id;
    private String title;
    private String detailImageUrl;
    private String description;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDetailImageUrl() {
        return detailImageUrl;
    }

    public String getDescription() {
        return description;
    }
}
