package com.abreu.moviesroad.data.entity;

import java.util.List;

public class MovieListEntity {
    private List<MovieEntity> movies;

    public List<MovieEntity> getMovies() {
        return movies;
    }
}
