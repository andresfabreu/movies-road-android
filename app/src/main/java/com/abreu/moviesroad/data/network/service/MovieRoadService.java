package com.abreu.moviesroad.data.network.service;

import com.abreu.moviesroad.data.entity.MovieDetailEntity;
import com.abreu.moviesroad.data.entity.MovieListEntity;
import com.abreu.moviesroad.data.entity.UserEntity;
import com.abreu.moviesroad.presentation.model.request.LoginRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MovieRoadService {

    @POST("auth/login")
    Call<UserEntity> doLogin(@Body LoginRequest loginRequest);

    @GET("movie/list")
    Call<MovieListEntity> getMovies(@Header("Authorization") String sessionToken);

    @GET("movie")
    Call<MovieDetailEntity> getMovieDetail(@Header("Authorization") String sessionToken, @Query("id") long movieId);
}
