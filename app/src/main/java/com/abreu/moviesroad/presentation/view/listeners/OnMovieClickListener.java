package com.abreu.moviesroad.presentation.view.listeners;

public interface OnMovieClickListener {
    void onMovieClicked(long movieId, String movieTitle);
}
