package com.abreu.moviesroad.presentation.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.abreu.moviesroad.R;
import com.abreu.moviesroad.data.entity.UserEntity;
import com.abreu.moviesroad.data.network.api.MovieRoadApi;
import com.abreu.moviesroad.presentation.model.request.LoginRequest;
import com.abreu.moviesroad.presentation.view.activity.MovieActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    @BindView(R.id.text_input_layout_username)
    TextInputLayout usernameTextInputLayout;

    @BindView(R.id.edit_text_username)
    TextInputEditText usernameEditText;

    @BindView(R.id.text_input_layout_password)
    TextInputLayout passwordTextInputLayout;

    @BindView(R.id.edit_text_password)
    TextInputEditText passwordEditText;

    @BindView(R.id.linear_layout_loading)
    LinearLayout loadingLayout;

    private MovieRoadApi movieRoadApi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        movieRoadApi = MovieRoadApi.getInstance();

        return view;
    }

    @OnClick(R.id.button_login)
    void onLoginButtonClicked() {
        if (TextUtils.isEmpty(usernameEditText.getText().toString())) {
            usernameTextInputLayout.setErrorEnabled(true);
            usernameTextInputLayout.setError(getString(R.string.invalid_username));
            return;
        }

        if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordTextInputLayout.setErrorEnabled(true);
            passwordTextInputLayout.setError(getString(R.string.invalid_password));
            return;
        }

        doLogin(usernameEditText.getText().toString(), passwordEditText.getText().toString());
    }

    @OnTextChanged(R.id.edit_text_username)
    void onUsernameTextChanged() {
        usernameTextInputLayout.setErrorEnabled(false);
        usernameTextInputLayout.setError("");
    }

    @OnTextChanged(R.id.edit_text_password)
    void onPasswordTextChanged() {
        passwordTextInputLayout.setErrorEnabled(false);
        passwordTextInputLayout.setError("");
    }

    private void doLogin(String username, String password) {
        showLoading();
        movieRoadApi.doLogin(new LoginRequest()
                .withUsername(username)
                .withPassword(password))
                .enqueue(new Callback<UserEntity>() {
                    @Override
                    public void onResponse(@NonNull Call<UserEntity> call, @NonNull Response<UserEntity> response) {
                        hideLoading();
                        if (response.isSuccessful()) {
                            UserEntity userEntity = response.body();
                            movieRoadApi.setSessionToken(userEntity != null ? userEntity.getAccessToken() : "");
                            startActivity(new Intent(getContext(), MovieActivity.class));
                        } else {
                            Toast.makeText(getContext(), "Erro", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UserEntity> call, @NonNull Throwable t) {
                        hideLoading();
                        Toast.makeText(getContext(), "Falha", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        loadingLayout.setVisibility(View.GONE);
    }

}
