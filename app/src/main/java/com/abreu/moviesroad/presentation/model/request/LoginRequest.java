package com.abreu.moviesroad.presentation.model.request;

public class LoginRequest {
    private String username;
    private String password;

    public LoginRequest withUsername(String username) {
        this.username = username;
        return this;
    }

    public LoginRequest withPassword(String password) {
        this.password = password;
        return this;
    }
}
